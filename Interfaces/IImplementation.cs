﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anydata.Interfaces
{
    interface ISingleton
    {

    }

    interface IScoped
    {

    }

    interface ITransient
    {

    }
}
