﻿using anydata.Entitys;
using Newtonsoft.Json.Linq;

namespace anydata.Interfaces
{
    public interface IPostInsert
    {
        Task Invoke(TokenModel token, string collectName, List<EntityBase> list);
    }    
    
    public interface IPostReplace
    {
        Task Invoke(TokenModel token, string collectName, List<EntityBase> list);
    }

    public interface IPostSetFields
    {
        Task Invoke(TokenModel token, string collectName, CollectSetFields data);
    }
}
