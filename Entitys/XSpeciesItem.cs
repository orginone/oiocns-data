namespace anydata.Entitys
{
    public class XSpeciesItem : EntityBase
    {
        public string info
        {
            get => this["info"] as string;
            set => this["info"] = value;
        }

        public string? speciesId
        {
            get => this["speciesId"] as string;
            set => this["speciesId"] = value;
        }

        public string? parentId
        {
            get => this["parentId"] as string;
            set => this["parentId"] = value;
        }
    }
}