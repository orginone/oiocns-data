namespace anydata.Entitys
{
    public class XAttribute : EntityBase
    {
        public string code
        {
            get => this["code"] as string;
            set => this["code"] = value;
        }
        public string rule
        {
            get => this["rule"] as string;
            set => this["rule"] = value;
        }
        public string queryRule
        {
            get => this["queryRule"] as string;
            set => this["queryRule"] = value;
        }
        public string propId
        {
            get => this["propId"] as string;
            set => this["propId"] = value;
        }
        public XProperty property
        {
            get => this["property"] as XProperty;
            set => this["property"] = value;
        }
        public string speciesId
        {
            get => this["speciesId"] as string;
            set => this["speciesId"] = value;
        }

        public FormField ToField()
        {
            var id = this.id ?? extensions["id"] as string;
            return new FormField
            {
                id = id,
                code = "T" + propId,
                attr = code,
                valueType = property.valueType,
                queryRule = queryRule
            };
        }
    }
}