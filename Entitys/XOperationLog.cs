﻿namespace anydata.Entitys
{
    public enum Status
    {
        Ready = 1,
        Working,
        Error,
        Stop,
        Completed
    }

    public class XOperationLog : EntityBase
    {
        public string? instanceId
        {
            get => this["instanceId"] as string;
            set => this["instanceId"] = value;
        }

        public string belongId
        {
            get => this["belongId"] as string;
            set => this["belongId"] = value;
        }

        public double? progress
        {
            get => this["progress"] as double?;
            set => this["progress"] = value;
        }

        public string typeName
        {
            get => this["typeName"] as string;
            set => this["typeName"] = value;
        }
    }
}
