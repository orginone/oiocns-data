namespace anydata.Entitys
{
    public class XForm : EntityBase
    {
        public List<XAttribute> attributes { get; set; }

        public List<FormField> fields { get; set; }
    }
}