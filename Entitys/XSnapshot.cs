﻿namespace anydata.Entitys
{
    public class XSnapshot : EntityBase
    {
        public string? instanceId
        {
            get => this["instanceId"] as string;
            set => this["instanceId"] = value;
        }

        public string? title
        {
            get => this["title"] as string;
            set => this["title"] = value;
        }

        public string thingId
        {
            get => this["thingId"] as string;
            set => this["thingId"] = value;
        }

        public XSnapshot(EntityBase? thing, XWorkInstance? instance) : base()
        {
            if (thing != null)
            {
                thingId = thing.id;
                foreach (var prop in thing.GetProperties())
                {
                    if (prop != "extensions")
                    {
                        this[prop] = thing[prop];
                    }
                }
            }
            if (instance != null)
            {
                instanceId = instance.id;
                title = instance.title;
            }
            createTime = DateTime.Now;
            updateTime = DateTime.Now;
            id = "snowId()";
        }
    }
}
