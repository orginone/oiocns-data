﻿namespace anydata.Entitys
{
    public class XPeriod : EntityBase
    {
        public string Period
        {
            get => this["period"] as string;
            set => this["period"] = value;
        }

        public bool? depreciated
        {
            get => this["depreciated"] as bool?;
            set => this["depreciated"] = value;
        }

        public bool? Closed
        {
            get => this["closed"] as bool?;
            set => this["closed"] = value;
        }

        public string operationId
        {
            get => this["operationId"] as string;
            set => this["operationId"] = value;
        }
    }
}
