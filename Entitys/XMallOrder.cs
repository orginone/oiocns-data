using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using SixLabors.ImageSharp;

namespace anydata.Entitys
{
    public class XMallOrder: EntityBase
    {

        /// <summary>
        /// 订单编号
        /// </summary>
        public string orderNumber
        {
            get => this["orderNumber"] as string;
            set => this["orderNumber"] = value;
        }
         
        /// <summary>
        /// 平台（集群）ID
        /// </summary>       
        public string platform
        {
            get => this["platform"] as string;
            set => this["platform"] = value;
        }
        /// <summary>
        /// 卖家单位ID
        /// </summary>
        public string seller
        {
            get => this["seller"] as string;
            set => this["seller"] = value;
        }
        /// <summary>
        /// 卖家单位名称
        /// </summary>
        public string sellerName
        {
            get => this["sellerName"] as string;
            set => this["sellerName"] = value;
        }
        /// <summary>
        /// 买家单位ID
        /// </summary>
        public string buyer
        {
            get => this["buyer"] as string;
            set => this["buyer"] = value;
        }
        /// <summary>
        /// 买家单位名称
        /// </summary>
        public string buyerName
        {
            get => this["buyerName"] as string;
            set => this["buyerName"] = value;
        }
        /// <summary>
        /// 商品信息
        /// </summary>
        public List<XGoods> itemList
        {
            get => this["itemList"] as List<XGoods>;
            set => this["itemList"] = value;
        }
        /// <summary>
        /// 实付价
        /// </summary>
        public double actualPaidPrice
        {
            get => this["actualPaidPrice"] as double? ?? 0;
            set => this["actualPaidPrice"] = value;
        }
        /// <summary>
        /// 原价
        /// </summary>
        public string originalPrice
        {
            get => this["originalPrice"] as string;
            set => this["originalPrice"] = value;
        }
        /// <summary>
        /// 折扣金额
        /// </summary>
        public double? discountAmount
        {
            get => this["discountAmount"] as double? ?? 0;
            set => this["discountAmount"] = value;
        }
        /// <summary>
        /// 折扣
        /// </summary>
        public string discount
        {
            get => this["discount"] as string;
            set => this["discount"] = value;
        }
        /// <summary>
        /// 商品总数
        /// </summary>
        public int totalCount
        {
            get => this["totalCount"] as int? ?? 0;
            set => this["totalCount"] = value;
        }
        /// <summary>
        /// 实付金额
        /// </summary>
        public string totalPrice
        {
            get => this["totalPrice"] as string;
            set => this["totalPrice"] = value;
        }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string orderStatus
        {
            get => this["orderStatus"] as string;
            set => this["orderStatus"] = value;
        }
        /// <summary>
        /// 订单时间
        /// </summary>
        public string orderTime
        {
            get => this["orderTime"] as string;
            set => this["orderTime"] = value;
        }
        /// <summary>
        /// 支付方式
        /// </summary>
        public string paymentMethod
        {
            get => this["paymentMethod"] as string;
            set => this["paymentMethod"] = value;
        }
        /// <summary>
        /// 交易id
        /// </summary>
        public string transactionId
        {
            get => this["transactionId"] as string;
            set => this["transactionId"] = value;
        }
        /// <summary>
        /// 支付时间
        /// </summary>
        public string paymentTime
        {
            get => this["paymentTime"] as string;
            set => this["paymentTime"] = value;
        }
        /// <summary>
        /// 买家用户id
        /// </summary>
        public string buyerUserId
        {
            get => this["buyerUserId"] as string;
            set => this["buyerUserId"] = value;
        }
        /// <summary>
        /// 买家用户名
        /// </summary>
        public string buyerUserName
        {
            get => this["buyerUserName"] as string;
            set => this["buyerUserName"] = value;
        }
        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string recipientName
        {
            get => this["recipientName"] as string;
            set => this["recipientName"] = value;
        }
        /// <summary>
        /// 收货人电话
        /// </summary>
        public string recipientPhone
        {
            get => this["recipientPhone"] as string;
            set => this["recipientPhone"] = value;
        }
        /// <summary>
        /// 收货地址
        /// </summary>
        public string deliveryAddress
        {
            get => this["deliveryAddress"] as string;
            set => this["deliveryAddress"] = value;
        }
        /// <summary>
        /// 买家备注
        /// </summary>
        public string buyerRemarks
        {
            get => this["buyerRemarks"] as string;
            set => this["buyerRemarks"] = value;
        }
    }
    /// <summary>
    /// 支付方式枚举
    /// </summary>
    public enum PaymentMethod {
        Alipay,        // 支付宝
        WECHAT_PAY,    // 微信支付
        BANK_TRANSFER, // 银行转账
        CREDIT_CARD,   // 信用卡
        COD,           // 货到付款
        DIGITAL_YUAN   // 数字人民币
    }
    // /// <summary>
    // /// 下单方式枚举
    // /// </summary>
    // public enum UsageType {
    //     SHARED_USAGE,  // 共享
    //     TRANSACTION,   // 交易
    //     BORROWING,     // 借用
    //     ADOPTION,      // 领用
    // }
    /// <summary>
    /// 下单状态枚举
    /// </summary>
    public enum OrderStatus {
        PROCESSING,         // 处理中
        PENDING_PAYMENT,    // 待支付
        PAID,               // 已支付
        PENDING_SHIPMENT,   // 待发货
        SHIPPED,            // 已发货
        COMPLETED,          // 已完成
        CANCELLED,          // 已取消
        RETURNED,           // 已退货
        RETURN_REQUESTED,   // 申请退货
        REFUNDED,           // 已退款
    }
}