using System.Globalization;

namespace anydata.Entitys
{
    public class XGoods : EntityBase
    {
		/// <summary>
        /// 归属单位id
        /// </summary>
		public string belongId
		{
			get => this["belongId"] as string;
		    set => this["belongId"] = value;
		}
		/// <summary>
        /// 商品名称
        /// </summary>
		public string title
		{
			get => this["title"] as string;
		    set => this["title"] = value;
		}
		/// <summary>
        /// 商品价格
        /// </summary>
		public double price
		{
			get => Convert.ToDouble(this["price"], CultureInfo.InvariantCulture);
		    set => this["price"] = value;
		}
		/// <summary>
        /// 购买商品数量
        /// </summary>
		public int purchaseQuantity
		{
			get => this["purchaseQuantity"] as int? ?? 0;
		    set => this["purchaseQuantity"] = value;
		}
		/// <summary>
        /// 商品数量
        /// </summary>
		public int count
		{
			get => this["count"] as int? ?? 0;
		    set => this["count"] = value;
		}
		/// <summary>
        /// 备注
        /// </summary>
		public string remark
		{
			get => this["remark"] as string;
		    set => this["remark"] = value;
		}
		/// <summary>
        /// 商品原始数据
        /// </summary>
		public string resource
		{
			get => this["resource"] as string;
		    set => this["resource"] = value;
		}
        public XGoods(EntityBase? instance) 
        {
            if (instance is null)
            {
                return;
            }
            foreach (var prop in instance.GetProperties())
            {
                this[prop] = instance[prop];
            }
        }
    }
}