﻿namespace anydata.Entitys
{
    public class XWorkInstance : EntityBase
    {
        public string? title
        {
            get => this["title"] as string;
            set => this["title"] = value;
        }
        
        public string? defineId
        {
            get => this["defineId"] as string;
            set => this["defineId"] = value;
        }        
        
        public string? applyId
        {
            get => this["applyId"] as string;
            set => this["applyId"] = value;
        }        
        
        public string? belongId
        {
            get => this["belongId"] as string;
            set => this["belongId"] = value;
        }

        public InstanceData? InstanceData { get; }


        public XWorkInstance() 
        {
            InstanceData = null;
        } 
        
        public XWorkInstance(EntityBase? instance) 
        {
            if (instance is null)
            {
                InstanceData = null;
                return;
            }

            foreach (var prop in instance.GetProperties())
            {
                this[prop] = instance[prop];
            }

            if (extensions.ContainsKey("data"))
            {
                InstanceData = InstanceData.Parse((instance.extensions["data"] as string) ?? "{}");
                extensions.Remove("data");
            }
        }
    }
}
