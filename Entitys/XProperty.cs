﻿namespace anydata.Entitys
{
    public class XProperty : EntityBase
    {
        public FormField field;

        public string valueType
        {
            get => this["valueType"] as string;
            set => this["valueType"] = value;
        }

        public string? speciesId
        {
            get => this["speciesId"] as string;
            set => this["speciesId"] = value;
        }

        public bool? isChangeSource
        {
            get => this["isChangeSource"] as bool?;
            set => this["isChangeSource"] = value;
        }

        public bool? isChangeTarget
        {
            get => this["isChangeTarget"] as bool?;
            set => this["isChangeTarget"] = value;
        }

        public void Initialize() {
            field = ToField();
        }

        public FormField ToField()
        {
            return new FormField
            {
                id = this.id,
                code = "T" + this.id,
                valueType = this.valueType,
                isChangeSource = this.isChangeSource,
                isChangeTarget = this.isChangeTarget,
            };
        }
    }

}
