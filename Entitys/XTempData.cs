using Newtonsoft.Json.Linq;

namespace anydata.Entitys
{
    public class XTempData : EntityBase
    {
        public string tempId { get; set; }
        public string thingId { get; set; }
        public EntityBase ToEntityBase()
        {
            return new EntityBase()
            {
                id = thingId,
                name = this.name,
                status = this.status,
                createUser = this.createUser,
                updateUser = this.updateUser,
                version = this.version,
                createTime = this.createTime,
                updateTime = this.updateTime,
                isDeleted = this.isDeleted,
                labels = this.labels,
                extensions = this.extensions,
            };
        }
    }
}