﻿using anydata.Loaders.Aggregation;
using anydata.Loaders.ResponseModel;
using anydata.Loaders.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anydata.Loaders.RemoteGrouping {

    class RemoteGroupTransformer {

        public static RemoteGroupingResult Run(Type sourceItemType, IEnumerable<AnonType> flatGroups, int groupCount, IReadOnlyList<SummaryInfo> totalSummary, IReadOnlyList<SummaryInfo> groupSummary) {
            List<Group> hierGroups = null;

            if(groupCount > 0) {
                hierGroups = new GroupHelper<AnonType>(AnonTypeAccessor.Instance).Group(
                    flatGroups,
                    Enumerable.Range(0, groupCount).Select(i => new GroupingInfo { selector = AnonType.IndexToField(1 + i) }).ToArray()
                );
            }

            IEnumerable dataToAggregate = hierGroups;
            if(dataToAggregate == null)
                dataToAggregate = flatGroups;

            var fieldIndex = 1 + groupCount;
            var transformedTotalSummary = TransformSummary(totalSummary, ref fieldIndex);
            var transformedGroupSummary = TransformSummary(groupSummary, ref fieldIndex);

            transformedTotalSummary = transformedTotalSummary ?? new List<SummaryInfo>();
            transformedTotalSummary.Add(new SummaryInfo { summaryType = AggregateName.REMOTE_COUNT });

            var sumFix = new SumFix(sourceItemType, totalSummary, groupSummary);
            var totals = new AggregateCalculator<AnonType>(dataToAggregate, AnonTypeAccessor.Instance, transformedTotalSummary, transformedGroupSummary, sumFix).Run();
            var totalCount = (int)totals.Last();

            totals = totals.Take(totals.Length - 1).ToArray();
            if(totals.Length < 1)
                totals = null;

            return new RemoteGroupingResult {
                Groups = hierGroups,
                Totals = totals,
                TotalCount = totalCount
            };
        }

        static List<SummaryInfo> TransformSummary(IReadOnlyList<SummaryInfo> original, ref int fieldIndex) {
            if(original == null)
                return null;

            var result = new List<SummaryInfo>();

            for(var originalIndex = 0; originalIndex < original.Count; originalIndex++) {
                var originalType = original[originalIndex].summaryType;

                if(originalType == AggregateName.COUNT) {
                    result.Add(new SummaryInfo {
                        summaryType = AggregateName.REMOTE_COUNT
                    });
                } else if(originalType == AggregateName.AVG) {
                    result.Add(new SummaryInfo {
                        summaryType = AggregateName.REMOTE_AVG,
                        selector = AnonType.IndexToField(fieldIndex)
                    });
                    fieldIndex += 2;
                } else {
                    result.Add(new SummaryInfo {
                        summaryType = originalType,
                        selector = AnonType.IndexToField(fieldIndex)
                    });
                    fieldIndex++;
                }
            }

            return result;
        }

    }

}
