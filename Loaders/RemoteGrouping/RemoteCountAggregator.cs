﻿using anydata.Loaders.Aggregation;
using anydata.Loaders.Helpers;
using anydata.Loaders.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anydata.Loaders.RemoteGrouping {

    class RemoteCountAggregator<T> : Aggregator<T> {
        int _count = 0;

        public RemoteCountAggregator(IAccessor<T> accessor)
            : base(accessor) {
        }

        public override void Step(T dataitem, string selector) {
            var group = dataitem as AnonType;
            _count += (int)group[0];
        }

        public override object Finish() {
            return _count;
        }
    }

}
