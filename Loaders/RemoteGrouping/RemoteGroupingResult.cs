﻿using anydata.Loaders.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anydata.Loaders.RemoteGrouping {

    class RemoteGroupingResult {
        public List<Group> Groups;
        public object[] Totals;
        public int TotalCount;
    }

}
