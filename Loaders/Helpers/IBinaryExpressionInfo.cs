﻿using System;
using System.Linq.Expressions;

namespace anydata.Loaders.Helpers {

    public interface IBinaryExpressionInfo {
        Expression DataItemExpression { get; }
        string AccessorText { get; }
        string Operation { get; }
        object Value { get; }
        bool StringToLower { get; }
    }

}
