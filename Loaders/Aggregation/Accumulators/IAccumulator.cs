﻿using System;
using System.Linq;

namespace anydata.Loaders.Aggregation.Accumulators {

    interface IAccumulator {
        void Add(object value);
        void Divide(int divider);
        object GetValue();
    }

}
