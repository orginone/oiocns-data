﻿using anydata.Loaders.Aggregation;
using anydata.Loaders.Helpers;
using anydata.Loaders.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace anydata.Loaders {

    partial class DataSourceLoadContext {
        readonly DataSourceLoadOptionsBase _options;
        readonly QueryProviderInfo _providerInfo;
        readonly Type _itemType;

        public DataSourceLoadContext(DataSourceLoadOptionsBase options, QueryProviderInfo providerInfo, Type itemType) {
            _options = options;
            _providerInfo = providerInfo;
            _itemType = itemType;
        }

        public bool GuardNulls {
            get {
                return _providerInfo.IsLinqToObjects;
            }
        }

        public bool RequireQueryableChainBreak {
            get {
                if(_providerInfo.IsXPO) {
                    // 1. XPQuery is stateful
                    // 2. CreateQuery(expr) and Execute(expr) don't spawn intermediate query instances for Queryable calls within expr.
                    //    This can put XPQuery into an invalid state. Example: Distinct().Count()
                    // 3. XPQuery is IQueryProvider itself
                    return true;
                }

                return false;
            }
        }

        public AnonTypeNewTweaks CreateAnonTypeNewTweaks() => new AnonTypeNewTweaks {
            AllowEmpty = !_providerInfo.IsL2S && !_providerInfo.IsMongoDB,
            AllowUnusedMembers = !_providerInfo.IsL2S
        };

        static bool IsEmpty<T>(IReadOnlyCollection<T> collection) {
            return collection == null || collection.Count < 1;
        }

        static bool IsEmptyList(IList list) {
            return list == null || list.Count < 1;
        }
    }

    // Total count
    partial class DataSourceLoadContext {
        public bool RequireTotalCount => _options.requireTotalCount;
        public bool IsCountQuery => _options.isCountQuery;
    }

    // Paging
    partial class DataSourceLoadContext {
        public int Skip => _options.skip;
        public int Take => _options.take;
        public bool HasPaging => Skip > 0 || Take > 0;
        public bool PaginateViaPrimaryKey => _options.paginateViaPrimaryKey.GetValueOrDefault(false);
    }

    // Filter
    partial class DataSourceLoadContext {
        public IList Filter => _options.filter;
        public bool HasFilter => !IsEmptyList(_options.filter);
        public bool UseStringToLower => _options.stringToLower ?? DataSourceLoadOptionsBase.stringToLowerDefault ?? _providerInfo.IsLinqToObjects;
        public bool SupportsEqualsMethod => !_providerInfo.IsXPO;
    }

    // Grouping
    partial class DataSourceLoadContext {
        bool?
            _shouldEmptyGroups,
            _useRemoteGrouping;

        public bool RequireGroupCount => _options.requireGroupCount;

        public IReadOnlyList<GroupingInfo> Group => _options.group;

        public bool HasGroups => !IsSummaryQuery && !IsEmpty(Group);

        public bool ShouldEmptyGroups {
            get {
                if(!_shouldEmptyGroups.HasValue)
                    _shouldEmptyGroups = HasGroups && !Group.Last().GetIsExpanded();
                return _shouldEmptyGroups.Value;
            }
        }

        public bool UseRemoteGrouping {
            get {

                bool HasAvg(IEnumerable<SummaryInfo> summary) {
                    return summary != null && summary.Any(i => i.summaryType == "avg");
                }

                bool ShouldUseRemoteGrouping() {
                    if(_providerInfo.IsLinqToObjects)
                        return false;

                    if(_providerInfo.IsEFCore) {
                        var version = _providerInfo.Version;

                        // https://github.com/aspnet/EntityFrameworkCore/issues/2341
                        // https://github.com/aspnet/EntityFrameworkCore/issues/11993
                        // https://github.com/aspnet/EntityFrameworkCore/issues/11999
                        if(version < new Version(2, 2, 0))
                            return false;

                        if(version.Major < 5) {
                            // https://github.com/aspnet/EntityFrameworkCore/issues/11711
                            if(HasAvg(TotalSummary) || HasAvg(GroupSummary))
                                return false;
                        }
                    }

                    return true;
                }

                if(!_useRemoteGrouping.HasValue)
                    _useRemoteGrouping = _options.remoteGrouping ?? ShouldUseRemoteGrouping();

                return _useRemoteGrouping.Value;
            }
        }
    }

    // Sorting & Primary Key
    partial class DataSourceLoadContext {
        bool _primaryKeyAndDefaultSortEnsured;
        string[] _primaryKey;
        string _defaultSort;

        public bool HasAnySort => HasGroups || HasSort || ShouldSortByPrimaryKey || HasDefaultSort;

        bool HasSort => !IsEmpty(_options.sort);

        public IReadOnlyList<string> PrimaryKey {
            get {
                EnsurePrimaryKeyAndDefaultSort();
                return _primaryKey;
            }
        }

        string DefaultSort {
            get {
                EnsurePrimaryKeyAndDefaultSort();
                return _defaultSort;
            }
        }

        public bool HasPrimaryKey => !IsEmpty(PrimaryKey);

        bool HasDefaultSort => !String.IsNullOrEmpty(DefaultSort);

        bool ShouldSortByPrimaryKey => HasPrimaryKey && _options.sortByPrimaryKey.GetValueOrDefault(true);

        public IEnumerable<SortingInfo> GetFullSort() {
            var memo = new HashSet<string>();
            var result = new List<SortingInfo>();

            if(HasGroups) {
                foreach(var g in Group) {
                    if(memo.Contains(g.selector))
                        continue;

                    memo.Add(g.selector);
                    result.Add(g);
                }
            }

            if(HasSort) {
                foreach(var s in _options.sort) {
                    if(memo.Contains(s.selector))
                        continue;

                    memo.Add(s.selector);
                    result.Add(s);
                }
            }

            IEnumerable<string> requiredSort = new string[0];

            if(HasDefaultSort)
                requiredSort = requiredSort.Concat(new[] { DefaultSort });

            if(ShouldSortByPrimaryKey)
                requiredSort = requiredSort.Concat(PrimaryKey);

            return Utils.AddRequiredSort(result, requiredSort);
        }

        void EnsurePrimaryKeyAndDefaultSort() {
            if(_primaryKeyAndDefaultSortEnsured)
                return;

            var primaryKey = _options.primaryKey;
            var defaultSort = _options.defaultSort;

            if(IsEmpty(primaryKey))
                primaryKey = Utils.GetPrimaryKey(_itemType);

            if(HasPaging && String.IsNullOrEmpty(defaultSort) && IsEmpty(primaryKey)) {
                if(_providerInfo.IsEFClassic || _providerInfo.IsEFCore)
                    defaultSort = EFSorting.FindSortableMember(_itemType);
                else if(_providerInfo.IsXPO)
                    defaultSort = "this";
            }

            _primaryKey = primaryKey;
            _defaultSort = defaultSort;
            _primaryKeyAndDefaultSortEnsured = true;
        }
    }

    // Summary
    partial class DataSourceLoadContext {
        bool? _summaryIsTotalCountOnly;

        public IReadOnlyList<SummaryInfo> TotalSummary => _options.totalSummary;

        public IReadOnlyList<SummaryInfo> GroupSummary => _options.groupSummary;

        public bool HasSummary => HasTotalSummary || HasGroupSummary;

        public bool HasTotalSummary => !IsEmpty(TotalSummary);

        public bool HasGroupSummary => HasGroups && !IsEmpty(GroupSummary);

        public bool SummaryIsTotalCountOnly {
            get {
                if(!_summaryIsTotalCountOnly.HasValue)
                    _summaryIsTotalCountOnly = !HasGroupSummary && HasTotalSummary && TotalSummary.All(i => i.summaryType == AggregateName.COUNT);
                return _summaryIsTotalCountOnly.Value;
            }
        }

        public bool ExpandLinqSumType {
            get {
                if(_options.expandLinqSumType.HasValue)
                    return _options.expandLinqSumType.Value;

                // NH 5.2.6: https://github.com/nhibernate/nhibernate-core/issues/2029
                // EFCore 2: https://github.com/aspnet/EntityFrameworkCore/issues/14851

                return _providerInfo.IsEFClassic
                    || _providerInfo.IsEFCore && _providerInfo.Version.Major > 2
                    || _providerInfo.IsXPO;
            }
        }

        public bool IsSummaryQuery => _options.isSummaryQuery;

        public bool IsRemoteTotalSummary => UseRemoteGrouping && !SummaryIsTotalCountOnly && HasSummary && !HasGroups;
    }

    // Select
    partial class DataSourceLoadContext {
        string[] _fullSelect;
        bool? _useRemoteSelect;

        public bool HasAnySelect => FullSelect.Count > 0;

        public bool UseRemoteSelect {
            get {
                if(!_useRemoteSelect.HasValue)
                    _useRemoteSelect = _options.remoteSelect ?? (!_providerInfo.IsLinqToObjects && FullSelect.Count <= AnonType.MAX_SIZE);

                return _useRemoteSelect.Value;
            }
        }

        public IReadOnlyList<string> FullSelect {
            get {
                string[] Init() {
                    var hasSelect = !IsEmpty(_options.select);
                    var hasPreSelect = !IsEmpty(_options.preSelect);

                    if(hasPreSelect && hasSelect)
                        return Enumerable.Intersect(_options.preSelect, _options.select, StringComparer.OrdinalIgnoreCase).ToArray();

                    if(hasPreSelect)
                        return _options.preSelect;

                    if(hasSelect)
                        return _options.select;

                    return new string[0];
                }

                if(_fullSelect == null)
                    _fullSelect = Init();

                return _fullSelect;
            }
        }
    }
}
