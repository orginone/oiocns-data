﻿using System.Linq;
using MongoDB.Driver;

namespace anydata.Loaders.Extensions
{
    public static class AggregateFluentExtensions
    {
        public static bool IsSorted<TResult>(this IAggregateFluent<TResult> aggregateFluent)
        {
            return aggregateFluent.Stages.Any(stage => stage.OperatorName == "$sort");
        }
    }
}