﻿using MongoDB.Driver;

namespace anydata.Loaders.Extensions
{
    public static class FindFluentExtensions
    {
        public static bool IsSorted<TDocument, TProjection>(this IFindFluent<TDocument, TProjection> findFluent)
        {
            return findFluent.Options.Sort != null;
        }
    }
}