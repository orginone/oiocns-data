﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace anydata.Loaders {

    /// <summary>
    /// A class with properties that specify data processing settings.
    /// </summary>
    public class DataSourceLoadOptionsBase {
        /// <summary>
        /// A global default value for the <see cref="stringToLower" /> property
        /// </summary>
        public static bool? stringToLowerDefault { get; set; }

        /// <summary>
        /// A flag indicating whether the total number of data objects is required.
        /// </summary>
        public bool requireTotalCount { get; set; }

        /// <summary>
        /// A flag indicating whether the number of top-level groups is required.
        /// </summary>
        public bool requireGroupCount { get; set; }

        /// <summary>
        /// A flag indicating whether the current query is made to get the total number of data objects.
        /// </summary>
        public bool isCountQuery { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool isSummaryQuery { get; set; }

        /// <summary>
        /// The number of data objects to be skipped from the start of the resulting set.
        /// </summary>
        public int skip { get; set; }

        /// <summary>
        /// The number of data objects to be loaded.
        /// </summary>
        public int take { get; set; }

        /// <summary>
        /// A sort expression.
        /// </summary>
        public SortingInfo[] sort { get; set; }

        /// <summary>
        /// A group expression.
        /// </summary>
        public GroupingInfo[] group { get; set; }

        /// <summary>
        /// A filter expression.
        /// </summary>
        public IList filter { get; set; }

        /// <summary>
        /// A total summary expression.
        /// </summary>
        public SummaryInfo[] totalSummary { get; set; }

        /// <summary>
        /// A group summary expression.
        /// </summary>
        public SummaryInfo[] groupSummary { get; set; }

        /// <summary>
        /// A select expression.
        /// </summary>
        public string[] select { get; set; }

        /// <summary>
        /// An array of data fields that limits the <see cref="select" /> expression.
        /// The applied select expression is the intersection of <see cref="preSelect" /> and <see cref="select" />.
        /// </summary>
        public string[] preSelect { get; set; }

        /// <summary>
        /// A flag that indicates whether the LINQ provider should execute the select expression.
        /// If set to false, the select operation is performed in memory.
        /// </summary>
        public bool? remoteSelect { get; set; }

        /// <summary>
        /// A flag that indicates whether the LINQ provider should execute grouping.
        /// If set to false, the entire dataset is loaded and grouped in memory.
        /// </summary>
        public bool? remoteGrouping { get; set; }

        public bool? expandLinqSumType { get; set; }

        /// <summary>
        /// An array of primary keys.
        /// </summary>
        public string[] primaryKey { get; set; }

        /// <summary>
        /// The data field to be used for sorting by default.
        /// </summary>
        public string defaultSort { get; set; }

        /// <summary>
        /// A flag that indicates whether filter expressions should include a ToLower() call that makes string comparison case-insensitive.
        /// Defaults to true for LINQ to Objects, false for any other provider.
        /// </summary>
        public bool? stringToLower { get; set; }

        /// <summary>
        /// If this flag is enabled, keys and data are loaded via separate queries. This may result in a more efficient SQL execution plan.
        /// </summary>
        public bool? paginateViaPrimaryKey { get; set; }

        public bool? sortByPrimaryKey { get; set; }

        public bool allowAsyncOverSync { get; set; }

        public string? formId { get; set; }
    }

}
