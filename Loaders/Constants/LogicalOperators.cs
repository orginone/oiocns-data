﻿namespace anydata.Loaders.Constants
{
    internal class LogicalOperators
    {
        public const string And = "and";
        public const string Or = "or";
    }
}