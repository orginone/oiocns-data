﻿namespace anydata.Loaders.Constants
{
    internal class ComparisonOperators
    {
        public const string Exists = "exists";
        public const string Equality = "=";
        public const string DoesNotEqual = "<>";
        public const string LessThan = "<";
        public const string GreaterThan = ">";
        public const string LessThanOrEqual = "<=";
        public const string GreaterThanOrEqual = ">=";
        public const string StartsWith = "startswith";
        public const string EndsWith = "endswith";
        public const string Contains = "contains";
        public const string NotContains = "notcontains";
    }
}