﻿using System;

namespace anydata.Loaders.Types {

    class DynamicClassAdapter : AnonType {
        readonly object Obj;

        public DynamicClassAdapter(object obj) {
            Obj = obj;
        }

        internal override object this[int index]
            => DynamicClassBridge.GetMember(Obj, index);

        protected override int Size
            => throw new NotSupportedException();

        public override bool Equals(object obj)
            => Obj.Equals(obj);

        public override int GetHashCode()
            => Obj.GetHashCode();
    }

}
