﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

using anydata.Loaders.Constants;

using MongoDB.Bson;
using MongoDB.Driver;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace anydata.Loaders.Builders
{
    internal static class FilterOptionBuilder<T>
    {
        private static readonly FilterDefinitionBuilder<T> Builder = Builders<T>.Filter;

        public static FilterDefinition<T> Build(IList filter)
        {
            var filterDefinition = FilterDefinition<T>.Empty;

            if (filter?.Count > 0)
            {
                var filterString = JsonConvert.SerializeObject(filter);
                var filterTree = JArray.Parse(filterString);

                filterDefinition = ReadExpression(filterTree);
            }

            return filterDefinition;
        }

        private static FilterDefinition<T> ReadExpression(JArray array)
        {
            if (!array.Any())
                return FilterDefinition<T>.Empty;

            var filterDefinition = FilterDefinition<T>.Empty;
            string currentLogicalOperator = string.Empty;

            if (array[0].Type == JTokenType.String)
                return BuildFromExpression(
                    array[0].ToString(),
                    array[1].ToString(),
                    array[2]);
            else
            {
                foreach (var item in array)
                {
                    if (item.Type == JTokenType.String) // logical, and, or vs
                    {
                        currentLogicalOperator = item.ToString();
                    }
                    else
                    {
                        FilterDefinition<T> exprFilter = ReadExpression((JArray)item);

                        filterDefinition = !string.IsNullOrEmpty(currentLogicalOperator)
                            ? BuildFromLogicalOperator(currentLogicalOperator, filterDefinition, exprFilter)
                            : exprFilter;

                        currentLogicalOperator = string.Empty;
                    }
                }
            }

            return filterDefinition;
        }

        private static FilterDefinition<T> BuildFromExpression(string columnName, string comparisonOperator, JToken value)
        {
            if (string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException(nameof(columnName));

            if (string.IsNullOrEmpty(comparisonOperator))
                throw new ArgumentNullException(nameof(comparisonOperator));

            FilterDefinition<T> result;
            BsonValue bsonValue = new BsonString(value.ToString());
            switch (value.Type)
            {
                case JTokenType.Boolean:
                    bsonValue = new BsonBoolean(value.Value<bool>());
                    break;
                case JTokenType.Date:
                    bsonValue = new BsonDateTime(value.Value<DateTime>());
                    break;
                case JTokenType.Integer:
                    bsonValue = new BsonInt64(value.Value<long>());
                    break;
                case JTokenType.Float:
                    bsonValue = new BsonDouble(value.Value<double>());
                    break;
            }
            switch (comparisonOperator)
            {
                case ComparisonOperators.Exists:
                    var exists = true;
                    if (bsonValue.BsonType == BsonType.Boolean)
                    {
                        exists = bsonValue.AsBoolean;
                    }
                    result = Builder.Exists(columnName, exists);
                    break;
                case ComparisonOperators.Equality:
                    if (value.Type == JTokenType.Null)
                    {
                        result = Builder.Or(Builder.Exists(columnName, false), Builder.Eq(columnName, bsonValue));
                    }
                    else
                    {
                        result = Builder.Eq(columnName, bsonValue);
                    }
                    break;
                case ComparisonOperators.DoesNotEqual:
                    if (value.Type == JTokenType.Null)
                    {
                        result = Builder.And(Builder.Exists(columnName, true), Builder.Ne(columnName, bsonValue));
                    }
                    else
                    {
                        result = Builder.Ne(columnName, bsonValue);
                    }
                    break;
                case ComparisonOperators.LessThan:
                    result = Builder.Lt(columnName, bsonValue);
                    break;
                case ComparisonOperators.GreaterThan:
                    result = Builder.Gt(columnName, bsonValue);
                    break;
                case ComparisonOperators.LessThanOrEqual:
                    result = Builder.Lte(columnName, bsonValue);
                    break;
                case ComparisonOperators.GreaterThanOrEqual:
                    result = Builder.Gte(columnName, bsonValue);
                    break;
                case ComparisonOperators.StartsWith:
                    result = Builder.Regex(columnName, new Regex($"^{value}.*", RegexOptions.IgnoreCase));
                    break;
                case ComparisonOperators.EndsWith:
                    result = Builder.Regex(columnName, new Regex($"{value}$", RegexOptions.IgnoreCase));
                    break;
                case ComparisonOperators.Contains:
                    result = Builder.Regex(columnName, new Regex($".*{value}.*", RegexOptions.IgnoreCase));
                    break;
                case ComparisonOperators.NotContains:
                    result = Builder.Not(Builder.Regex(columnName, new Regex($".*{value}.*", RegexOptions.IgnoreCase)));
                    break;
                default:
                    throw new NotImplementedException();
            }

            return result;
        }

        private static FilterDefinition<T> BuildFromLogicalOperator(string logicalOperator, params FilterDefinition<T>[] filterDefinitions)
        {
            if (string.IsNullOrEmpty(logicalOperator))
                throw new ArgumentNullException(nameof(logicalOperator));

            if (filterDefinitions.Count() == 1)
                return filterDefinitions[0];

            if (logicalOperator == LogicalOperators.And)
                return Builder.And(filterDefinitions);

            if (logicalOperator == LogicalOperators.Or)
                return Builder.Or(filterDefinitions);

            throw new NotImplementedException();
        }
    }
}