var builder = WebApplication.CreateBuilder(args);
builder.Configuration.Initialization();
builder.Logging.ClearProviders();
builder.Logging.AddConsole();
builder.Logging.SetMinimumLevel(Config.LogLevel);
builder.Services.AddResponseCaching();
builder.Services.AddResponseCompression();
builder.Services.AddControllers().AddNewtonsoftJson(option =>
{
    option.SerializerSettings.Setting();
});
builder.Services.AddSignalR(option =>
{
    option.StreamBufferCapacity = 1024;
    option.KeepAliveInterval = TimeSpan.FromMilliseconds(Config.KeepAliveInterval);
    option.HandshakeTimeout = TimeSpan.FromMilliseconds(Config.KeepTimeout);
    option.ClientTimeoutInterval = TimeSpan.FromMilliseconds(Config.KeepTimeout);
    option.MaximumReceiveMessageSize = 100 * 1024 * 1024;
}).AddNewtonsoftJsonProtocol(option =>
{
    option.PayloadSerializerSettings.Setting();
});
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddLocalServices();
builder.Services.AddHookServices();
builder.Services.AddHostedService<KernelHubWorker>();

var app = builder.Build();

app.UseStatusCodePages(ErrorHanlder.StatusCode);
app.UseExceptionHandler(ErrorHanlder.Exception);

app.UseCors();
app.UseRouting();
app.UseResponseCaching();
app.UseResponseCompression();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapProxy("/orginone/anydata/proxy/http");
    endpoints.MapHub<DataHub>("/orginone/anydata/hub");
});

var runTask = app.RunAsync(Config.Urls);
foreach (var url in app.Urls)
{
    Console.WriteLine($"Starting server at {url}");
}
runTask.Wait();