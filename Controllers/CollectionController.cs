﻿using anydata.Entitys;
using anydata.Handlers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;

using Newtonsoft.Json.Linq;


namespace anydata.Controllers
{
    [ApiController]
    [Route("orginone/anydata/[controller]/[action]")]
    public class CollectionController : ControllerBase
    {
        readonly DataProvider _provider;
        public CollectionController(DataProvider provider)
        {
            _provider = provider;
        }

        [HttpGet()]
        [HttpPost()]
        public Task<IActionResult> ListAsync(TokenModel _token)
        {
            return _provider.ListAsync(_token).TryExecuteResult();
        }
        [HttpPost("{collectName}")]
        public Task<IActionResult> LoadAsync(TokenModel _token, string collectName, [FromBody] DataSourceLoadOptions options)
        {
            return _provider.LoadAsync(_token, collectName, options).TryExecuteResult();
        }
        [HttpPost("{collectName}")]
        public Task<IActionResult> AggregateAsync(TokenModel _token, string collectName, [FromBody] JToken options)
        {
            return _provider.AggregateAsync(_token, collectName, options).TryExecuteResult();
        }

        [HttpPost("{collectName}")]
        public Task<IActionResult> InsertAsync(TokenModel _token, string collectName, [FromBody] JToken data)
        {
            return _provider.InsertAsync(_token, collectName, data).TryExecuteResult();
        }

        [HttpPost("{collectName}")]
        public Task<IActionResult> SetFieldsAsync(TokenModel _token, string collectName, [FromBody] CollectSetFields data)
        {
            return _provider.SetFieldsAsync(_token, collectName, data).TryExecuteResult();
        }

        [HttpPost("{collectName}")]
        public Task<IActionResult> UpdateAsync(TokenModel _token, string collectName, [FromBody] CollectUpdateData data)
        {
            return _provider.UpdateAsync(_token, collectName, data).TryExecuteResult();
        }

        [HttpPost("{collectName}")]
        public Task<IActionResult> RemoveAsync(TokenModel _token, string collectName, [FromBody] JToken match)
        {
            return _provider.RemoveAsync(_token, collectName, match).TryExecuteResult();
        }

        public Task<IActionResult> LoadThingAsync(TokenModel _token, [FromBody] DataSourceLoadOptions options)
        {
            return _provider.LoadAsync(_token, string.Empty, options).TryExecuteResult();
        }

        public Task<JsonResult> CreateThingAsync(TokenModel _token, [FromBody] string name)
        {
            return Task.FromResult(OperateResult.Success(new EntityBase()
            {
                name = name,
                id = SnowflakeIDcreator.nextId().ToString(),
            }).Result());
        }

        public Task<IActionResult> DestroyThingAsync(TokenModel _token, [FromBody] List<string> Ids)
        {
            return _provider.DestroyAsync(_token, Ids).TryExecuteResult();
        }
    }
}
