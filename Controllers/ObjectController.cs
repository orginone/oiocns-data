﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

using Newtonsoft.Json.Linq;

namespace anydata.Controllers
{
    [ApiController]
    [Route("orginone/anydata/[controller]/[action]")]
    public class ObjectController : ControllerBase
    {
        readonly DataProvider _provider;
        public ObjectController(DataProvider provider)
        {
            _provider = provider;
        }

        [HttpGet("{objectName?}")]
        [HttpPost("{objectName?}")]
        public Task<IActionResult> ListAsync(TokenModel _token, string? objectName)
        {
            return _provider.ListAsync(_token, objectName ?? "").TryExecuteResult();
        }

        [HttpGet("{objectName}")]
        [HttpPost("{objectName}")]
        public Task<IActionResult> GetAsync(TokenModel _token, string objectName)
        {
            return _provider.GetAsync(_token, objectName).TryExecuteResult();
        }

        [HttpPost("{objectName}")]
        public Task<IActionResult> SetAsync(TokenModel _token, string objectName, [FromBody] ObjectSetData objectValue)
        {
            return _provider.SetAsync(_token, objectName, objectValue).TryExecuteResult();
        }

        [HttpDelete("{objectName}")]
        [HttpPost("{objectName}")]
        public Task<IActionResult> DeleteAsync(TokenModel _token, string objectName)
        {
            return _provider.DeleteAsync(_token, objectName).TryExecuteResult();
        }
    }
}
