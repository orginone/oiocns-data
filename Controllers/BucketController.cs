﻿using anydata.DataStorage;
using anydata.Handlers;

using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json.Linq;

namespace anydata.Controllers
{
    [ApiController]
    [Route("orginone/anydata/[controller]/[action]")]
    public class BucketController : ControllerBase
    {
        private BucketManager _bucket;
        public BucketController()
        {
            _bucket = new BucketManager();
        }

        [HttpGet("{link}")]
        public Task<IActionResult> Share(string link)
        {
            if (link.Contains("0")) {
                link = link.Substring(link.IndexOf("0") + 1);
            }
            return _bucket.ExecuteResult(new TokenModel(), new BucketItemData()
            {
                key = link.Substring(10).FromBase32().ToBase64(),
                operate = SupportOpreate.Download
            });
        }

        [HttpPost]
        public Task<IActionResult> Operate(TokenModel _token, [FromBody]BucketItemData item)
        {
            return _bucket.ExecuteResult(_token, item);
        }
    }
}
