﻿using anydata.Entitys;
using anydata.Handlers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;

using Newtonsoft.Json.Linq;


namespace anydata.Controllers
{
    [ApiController]
    [Route("orginone/anydata/[controller]/[action]")]
    public class PublicController : ControllerBase
    {
        readonly DataProvider _provider;
        public PublicController(DataProvider provider)
        {
            _provider = provider;
        }
        [HttpPost()]
        public Task<IActionResult> CreateOrderAsync(TokenModel _token, [FromBody] MallOrderData data)
        {
            return _provider.CreateOrderAsync(_token, data).TryExecuteResult();
        }

    }
}
