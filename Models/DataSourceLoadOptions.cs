﻿using System.Threading.Tasks;
using anydata.Loaders;
using anydata.Loaders.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Newtonsoft.Json.Linq;

namespace anydata.Models
{

    //[ModelBinder(typeof(DataSourceLoadOptionsBinder))]
    public class DataSourceLoadOptions : DataSourceLoadOptionsBase
    {
        public DataSourceLoadOptions()
        {
            userData = new List<string>();
        }
        public List<string> userData { get; set; }
        public JToken options { get; set; }
        public string collName { get; set; }
    }

    //internal class DataSourceLoadOptionsBinder : IModelBinder
    //{
    //    public async Task BindModelAsync(ModelBindingContext bindingContext)
    //    {
    //        var loadOptions = new DataSourceLoadOptions();
    //        await Task.Run(() => DataSourceLoadOptionsParser.Parse(loadOptions, key => bindingContext.ValueProvider.GetValue(key).FirstValue));
    //        bindingContext.Result = ModelBindingResult.Success(loadOptions);
    //    }
    //}
}