﻿using anydata.Entitys;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace anydata.Models
{
    public class MallOrderData
    {
        [JsonProperty("data")]
        public XMallOrder Data { get; set; }

        public string Check(ulong id)
        {
            if(!Enum.TryParse<OrderStatus>(Data.orderStatus, true, out var orderStatus)) {
                return $"订单状态字段orderStatus的数据格式不正确！";
            }
            if(Data.totalCount<=0) {
                return $"数量字段totalCount的值要大于0！";
            }
            ulong buyer,seller,platform;
            ulong.TryParse(Data.buyer, out buyer);
            ulong.TryParse(Data.seller, out seller);
            ulong.TryParse(Data.platform, out platform);
            if(buyer!=id&&seller!=id&&platform!=id) {
                return $"下单单位或供货单位Code字段buyer|seller的值要等于当前用户所在单位Id！";
            }
            return null;
        }
    }
}
