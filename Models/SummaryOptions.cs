using Newtonsoft.Json.Linq;

namespace anydata.Models;

public class SummaryOptions
{
    public string collName { get; set; } = "";

    public List<string> fields { get; set; } = new();

    public JToken? match { get; set; }

    public List<string> ids { get; set; } = new();

    public int chunkSize { get; set; } = 2000;
}