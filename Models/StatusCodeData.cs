﻿namespace anydata.Models
{
    public class StatusCodeData
    {
        public string Host { get; set; }
        public string Scheme { get; set; }
        public string Method { get; set; }
        public int StatusCode { get; set; }
        public string Protocol { get; set; }
        public string RequestPath { get; set; }
        public string QueryString { get; set; }
    }
}
