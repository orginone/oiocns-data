﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace anydata.Models
{
    public class CollectUpdateData
    {
        [JsonProperty("match")]
        public JToken Match { get; set; }

        [JsonProperty("update")]
        public JToken Update { get; set; }
        [JsonProperty("options")]
        public JToken? Options { get; set; }
    }

    public class CollectSetFields
    {
        public CollectSetFields()
        {
            Ids = new List<string>();
        }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("ids")]
        public List<string> Ids { get; set; }

        [JsonProperty("update")]
        public JToken Update { get; set; }
        [JsonProperty("options")]
        public JToken? Options { get; set; }
    }
}
