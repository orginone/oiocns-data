﻿using MongoDB.Bson;

namespace anydata.Models
{
    public class MongoCommands
    {
        public static List<string> StatsNames = new List<string>()
        {
            "size","count","avgObjSize","storageSize","totalIndexSize",
            "totalSize", "max","maxSize"
        };
    }
}
