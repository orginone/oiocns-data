﻿using anydata.Entitys;

namespace anydata.Models
{
    public class ChangesAndSnapshots
    {
        public ChangesAndSnapshots(List<XChange> changes, List<XSnapshot> snapshots) { 
            this.changes = changes;
            this.snapshots = snapshots;
        }

        public List<XChange> changes;
        public List<XSnapshot> snapshots;

        public void Deconstruct(out List<XChange> changes, out List<XSnapshot> snapshots)
        {
           changes = this.changes;
           snapshots = this.snapshots;
        }
    }
}
