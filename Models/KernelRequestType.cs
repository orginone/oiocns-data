﻿using Newtonsoft.Json;

namespace anydata.Models
{
    public class KernelRequestType
    {
        public string flag { get; set; }
        public string module { get; set; }
        public string action { get; set; }
        public ulong userId { get; set; }
        public ulong targetId { get; set; }
        public ulong belongId { get; set; }
    }
    public class KernelRequestType<T> : KernelRequestType
    {
        [JsonProperty("params")]
        public T args { get; set; }
    }
}
