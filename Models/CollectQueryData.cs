﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace anydata.Models
{
    public class CollectQueryData
    {
        [JsonProperty("match")]
        public JToken Match { get; set; }

        [JsonProperty("sort")]
        public JToken Sort { get; set; }

        [JsonProperty("replaceRoot")]
        public JToken ReplaceRoot { get; set; }

        [JsonProperty("lookup")]
        public CollectLookup Lookup { get; set; }

        [JsonProperty("project")]
        public JToken Project { get; set; }

        [JsonProperty("group")]
        public JToken Group { get; set; }

        [JsonProperty("skip")]
        public int Skip { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("orders")]
        public List<string> Orders { get; set; }

        public void Check()
        {
            Limit = Limit > 1000000 ? 1000000 : Limit;
            if(Orders == default || Orders.Count < 1)
            {
                Orders = new List<string>()
                {
                    "match","project","group","sort","skip","limit", "lookup", "replaceRoot"
                };
            }
        }
    }

    public class CollectLookup
    {
        public string from { get; set; }
        public string localField { get; set; }
        public string foreignField { get; set; }
        [JsonProperty("as")]
        public string filed { get; set; }
    }
}
