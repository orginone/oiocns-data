﻿using Newtonsoft.Json;
using System.Threading.Channels;

namespace anydata.Models
{
    public class HubFileResult
    {
        public HubFileResult()
        {
            end = true;
            errorMsg = string.Empty;
            statusCode = 200;
            contentLength = 0;
        }
        public bool end { get; set; }
        public string errorMsg { get; set; }
        public int statusCode { get; set; }
        public string fileName { get;set; }
        public long contentLength { get; set; }
        public string contentType { get; set; }
        public string modTime { get; set; }
        public string content { get; set; }
        public long startIndex { get; set; }
        public long endIndex { get; set; }
    }
}
