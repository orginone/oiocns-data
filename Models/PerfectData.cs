﻿using Newtonsoft.Json.Linq;

using System.Text.Json.Serialization;

namespace anydata.Models
{
    public class PerfectData
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("createUser")]
        public ulong CreateUser { get; set; }
        [JsonPropertyName("belongId")]
        public ulong BelongId { get; set; }
        [JsonPropertyName("shareId")]
        public ulong ShareId { get; set; }
        [JsonPropertyName("status")]
        public int Status { get; set; }
    }

    public class SetDataModel
    {
        [JsonPropertyName("propertys")]
        public Dictionary<string, object> Propertys { get; set; }
        [JsonPropertyName("species")]
        public Dictionary<string, string> Species { get; set; }
    }
}
