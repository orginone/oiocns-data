﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace anydata.Models
{
    public class ObjectSetData
    {
        [JsonProperty("operation")]
        public string Operation { get; set; }

        [JsonProperty("data")]
        public JToken Data { get; set; }

        public JToken GetData()
        {
            Data = Data ?? new JObject();
            if (Operation == "append" && Data.Type == JTokenType.Object)
            {
                var retVal = new JArray();
                retVal.Add(Data);
                return retVal;
            }
            return Data;
        }

        [JsonIgnore]
        public bool IsObject
        {
            get
            {
                return GetData().Type == JTokenType.Object;
            }
        }
    }
}
