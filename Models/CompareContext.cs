﻿using anydata.Entitys;

namespace anydata.Models
{
    public class CompareContext
    {
        private static readonly List<string> sourceTags = new() { "选择型", "分类型", "用户型" };

        public CompareContext(List<FormField> fields)
        {
            Fields = fields;
            TargetFields = fields.Where(item => item.isChangeTarget == true && item.valueType == "数值型").DistinctBy(item => item.id).ToList();
            SourceFields = fields.Where(item => item.isChangeSource == true && sourceTags.Contains(item.valueType)).DistinctBy(item => item.id).ToList();
        }

        public TokenModel Token { get; set; }
        public string ChangeTime { get; set; }
        public XWorkInstance Instance { get; set; } 
        public List<FormField> Fields { get; set; }
        public List<FormField> TargetFields { get; set; }
        public List<FormField> SourceFields { get; set; }
        public bool HasRecord { get { return TargetFields.Count > 0; } }
    }
}
