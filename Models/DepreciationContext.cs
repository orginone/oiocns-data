﻿using anydata.Entitys;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System;

namespace anydata.Models
{
    public class DepreciationContext
    {
        public DepreciationContext(TokenModel token, XDepreciationConfig config, XPeriod period, DepreciationArgs args)
        {
            Token = token;
            Config = config;
            Current = period;
            Instance = new XWorkInstance() { id = period.id, title = "计提折旧" };
            OperationLog = new XOperationLog()
            {
                id = SnowflakeIDcreator.nextId().ToString(),
                extensions = new Dictionary<string, object>(),
                instanceId = Instance.id,
                name = Instance.title,
                belongId = token.BelongId.ToString(),
                createUser = token.UserId.ToString(),
                updateUser = token.UserId.ToString(),
                progress = 0,
                typeName = args.Type
            };
        }

        public TokenModel Token { get; set; }
        public XPeriod Current { get; set; }
        public XWorkInstance Instance { get; set; }
        public XOperationLog OperationLog { get; set; }
        public XDepreciationConfig Config { get; set; }
        public DateTime BusinessTime { get; set; }

        public JToken BuildWorkQuery()
        {
            return new JObject()
            {
                ["match"] = BuildMatch()
            };
        }

        public JToken BuildMatch()
        {
            return new JObject()
            {
                ["instanceId"] = Instance.id,
                ["belongId"] = Token.BelongId.ToString()
            };
        }
    }
}
