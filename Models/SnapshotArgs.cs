﻿namespace anydata.Models
{
    public record class SnapshotArgs
    {
        public string collName { get; set; }
        public string dataPeriod { get; set; }
    }
}
