﻿namespace anydata.Models
{
    public class DepreciationArgs
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }
}
