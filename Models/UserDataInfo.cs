﻿namespace anydata.Models
{
    public class UserDataInfo
    {
        public long ok { get; set; } = -1;
        public long objects { get; set; } = 0;
        public long collections { get; set; } = 0;
        public long dataSize { get; set; } = 0;
        public long totalSize { get; set; } = 0;
        public long filesCount { get; set; } = 0;
        public long filesSize { get; set; } = 0;
        public long fsUsedSize { get; set; } = 0;
        public long fsTotalSize { get; set; } = 0;
        public DateTime getTime { get; set; } = DateTime.Now;
    }
}
