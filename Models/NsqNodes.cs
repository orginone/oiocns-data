﻿namespace anydata.Models
{
    public class NsqNodes
    {
        public NsqNodes()
        {
            channels = new List<string>();
            producers = new List<ProducerModel>();
        }
        public List<string> channels { get; set; }
        public List<ProducerModel> producers { get; set; }
        public List<string> GetNsqdHttps()
        {
            return producers.Select(i => $"{i.broadcast_address}:{i.http_port}").ToList();
        }

        public List<string> GetNsqdTcps()
        {
            return producers.Select(i => $"{i.broadcast_address}:{i.tcp_port}").ToList();
        }
    }

    public class ProducerModel
    {
        public string hostname { get; set; }
        public int tcp_port { get; set; }
        public int http_port { get; set; }
        public string broadcast_address { get; set; }
    }
}
