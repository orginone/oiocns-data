﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace anydata.Models
{
    public enum SupportOpreate
    {
        Info,
        List,
        Create,
        Rename,
        Move,
        Copy,
        Delete,
        Upload,
        Download,
        HslSplit,
        AbortUpload,
    }
    public class BucketConfig
    {
        public BucketConfig(string b, string path,  long cSize, string ffmpegPath)
        {
            var root = new DirectoryInfo(b);
            temp = root.CreateSubdirectory(".temp").FullName;
            if (path == "")
            {
                user = root.FullName;
            }
            else
            {
                user = root.CreateSubdirectory(path).FullName;
            }
            chunkSize = cSize;
            this.ffmpegPath = ffmpegPath;
        }
        public long chunkSize { get; set; }
        public string user { get; set; }
        public string temp { get; set; }
        public string ffmpegPath { get; set; }
    }
    public class BucketItemData
    {
        public string key { get; set; }
        public string? name { get; set; }
        public SupportOpreate operate { get; set; }
        public string? destination { get; set; }
        public FileItemData? fileItem { get; set; }
        public ulong targetId { get; set; }

        public string GetName()
        {
            if(name == null || name.Trim().Length < 1)
            {
                throw new Exception("文件名不能为空");
            }
            name = name.Trim();
            return name;
        }
        public string GetDestination(string basePath)
        {
            if (destination == null || destination.Trim().Length < 1)
            {
                throw new Exception("目标目录不能为空");
            }
            destination = destination.Trim();
            return preparePath(basePath, destination);
        }

        public string KeyPath(string basePath)
        {
            return preparePath(basePath, key.FromBase64());
        }

        public FileItemData GetData(long chunkSize)
        {
            if(fileItem == null)
            {
                throw new Exception("未携带文件数据");
            }
            if(fileItem.data.Length == 0)
            {
                var index = fileItem.dataUrl.IndexOf(',');
                if(index > 0)
                {
                    fileItem.dataUrl = fileItem.dataUrl.Substring(index);
                }
                fileItem.data = Convert.FromBase64String(fileItem.dataUrl);
            }
            var totalCount = (int)Math.Ceiling(fileItem.size * 1.0 / chunkSize);
            fileItem.lastChunk = fileItem.index == totalCount - 1;
            if (!fileItem.lastChunk && fileItem.data.Length != chunkSize)
            {
                throw new Exception("分片大小为" + chunkSize + "个字节");
            }
            return fileItem;
        }

        private string preparePath(string basePath, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return basePath;
            }
            List<string> list = path.Split('\\', '/', StringSplitOptions.RemoveEmptyEntries).ToList();
            int num = 0;
            while (num < list.Count)
            {
                if (list[num] == ".." && num > 0)
                {
                    list.RemoveAt(num);
                    list.RemoveAt(num - 1);
                    num--;
                }
                else
                {
                    num++;
                }
            }
            if (list.Any() && list[0] == "..")
            {
                throw new Exception("操作不支持!");
            }
            list.Insert(0, basePath);
            return Path.Combine(list.ToArray());
        }
    }

    public class FileItemData
    {
        public FileItemData()
        {
            data = new byte[0];
        }
        public long index { get; set; }
        public long size { get; set; }
        public string uploadId { get; set; }
        public byte[] data { get; set; }
        public string dataUrl { get; set; }
        public bool lastChunk { get; set; }
    }

    public class FileSysItem
    {
        public long size { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public string poster { get; set; }
        public string shareLink { get; set; }
        public bool isDirectory { get; set; }
        public string thumbnail { get; set; }
        public string extension { get; set; }
        public string contentType { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateModified { get; set; }
        public bool hasSubDirectories { get; set; }
    }

    public class DiskInfoItem
    {
        public long size { get; set; } = 0;
        public long fileCount { get; set; } = 0;
        public DateTime lastTime { get; set; } = DateTime.Now;
    }
}
