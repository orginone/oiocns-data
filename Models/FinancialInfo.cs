﻿namespace anydata.Models
{ 
    public record class FinancialInfo
    {
        public string? initialized { get; set; }
        public string? current { get; set; }

        public void Deconstruct(out string? initialized, out string? current)
        {
            initialized = this.initialized;
            current = this.current;
        }
    }
}
