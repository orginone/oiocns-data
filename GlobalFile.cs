﻿global using anydata.Extensions;
global using anydata.Providers;
global using anydata.Models;
global using anydata.DataShare;
global using anydata.Interfaces;
global using anydata.Handlers;
global using anydata.Wrokers;
using System.Text;

public static class Config
{
    public static void Initialization(this ConfigurationManager config)
    {
        config.AddYamlFile("config.yaml");
        Urls = config.GetValue<string>("Urls");
        Bucket = config.GetValue<string>("Bucket");
        WorkerId = config.GetValue<uint>("WorkerId");
        KernelNum = config.GetValue<int>("KernelNum");
        LogLevel = config.GetValue<LogLevel>("LogLevel");
        AccessExpire = config.GetValue<int>("AccessExpire");
        FfmpegPath = config.GetValue<string>("FfmpegPath");
        KernelAddr = config.GetValue<string>("KernelAddr");
        KernelAuth = config.GetValue<string>("KernelAuth");
        ConnectionKey = config.GetValue<string>("ConnectionKey");
        ExecutorTimeout = config.GetValue<int>("ExecutorTimeout");
        KeepTimeout = config.GetValue<int>("KeepTimeout");
        KeepAliveInterval = config.GetValue<int>("KeepAliveInterval");
        ConnectionString = config.GetValue<string>("ConnectionString");
    }
    public static string Urls { get; private set; }
    public static string Bucket { get; private set; }
    public static uint WorkerId { get; private set; }
    public static int KernelNum { get; private set; }
    public static int AccessExpire { get; private set; }
    public static string KernelAddr { get; private set; }
    public static string KernelAuth { get; private set; }
    public static LogLevel LogLevel { get; private set; }
    public static string FfmpegPath { get; private set; }
    public static int KeepTimeout { get; private set; }
    public static int KeepAliveInterval { get; private set; }
    public static int ExecutorTimeout { get; private set; }
    public static string ConnectionKey { get; private set; }
    public static string ConnectionString { get; private set; }
    public static string DbConnectionString()
    {
        if (string.IsNullOrWhiteSpace(ConnectionString))
        {
            if (!string.IsNullOrWhiteSpace(ConnectionKey) && File.Exists(ConnectionKey))
            { 
                return ParseConnectionString(File.ReadAllText(ConnectionKey));
            }
        } 
        return ParseConnectionString(ConnectionString);
    }

    private static string ParseConnectionString(string ConnectionString)
    {
        if (!string.IsNullOrWhiteSpace(ConnectionString) && ConnectionString.Length > 10)
        {
            try
            {
                ConnectionString = ConnectionString.Substring(5); 
                return Encoding.UTF8.GetString(Convert.FromBase64String(ConnectionString));
            }
            catch { }
        }
        return "";
    }
}