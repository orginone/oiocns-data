# 奥集能数据核

![Image text](https://user-images.githubusercontent.com/8328012/201800690-9f5e989e-4ed3-4817-85b9-b594ac89fd31.png)

## 架构简介
面向下一代互联网发展趋势，基于动态演化的复杂系统多主体建模方法，以所有权作为第一优先级，运用零信任安全机制，按自组织分形理念提炼和抽象“沟通、办事、门户、数据、关系”等基础功能，为b端和c端融合的全场景业务的提供新一代分布式应用架构。    
## 基本功能
### 奥集能
奥集能（Orginone 发音[ˈɔːdʒɪnʌn]）个人和组织数字化一站式解决方案！   
奥：奥妙，莫名其妙。集，聚集，无中生有。能，赋能，点石成金。   
### 门户
按权限自定义工作台、动态信息，新闻资讯，交易商城，监控大屏，驾驶舱等各类页面。以用户为中心，汇聚各类数据和信息。
### 沟通
为个人和组织提供可靠、安全、私密的即时沟通工具，好友会话隐私保护作为第一优先级，同事和组织等工作会话单位数据权利归属优先。   
### 办事
满足个人、组织和跨组织协同办事需求，适应各类业务流程场景，支持发起、待办、已办、抄送、归档等不同状态流程类业务审核审批和查询。      
### 数据
用户对数据标准和存储方式拥有绝对控制权，自主选择存储资源，自定义数据标准、业务模型和管理流程，无代码配置应用，便捷迁移外部数据，支持通用文件系统管理功能。   
### 关系
支持个人和组织的关系的建立，好友和成员的管理，家庭、群组、单位、部门、集团等各类组织形态的构建，快速将工作和业务关系数字化、在线化，支持灵活的权限、角色和岗位管理等不同颗粒度的访问控制功能。   

### 本存储是奥集能数据核（可信数据空间） C# aspnetcore 的实现
- 体验地址：https://ocia.orginone.cn 
- 注册账号后可以申请加入一起研究群：research，协同研发群：asset_devops

### 项目依赖环境、安装和运行

奥集能是基于 aspnetcore-6.0实现，运行代码本地安装aspnetcore-runtime-6.0, 参与开发本地安装aspnetcore-sdk-6.0,vscode用户可以安装.net install tool，c# dev kit等插件配置开发环境。
#### 登录前端，在关系中右键，设立存储
#### 通过接口获取KernelAuth字符串，
```
POST {{url}}/auth HTTP/1.1
Content-Type: application/json

{
    "module": "auth",
    "action": "GetDataKey",
    "params": {
        "account": "账号",
        "password": "密码", //可选 // 密码,手机验证码,私钥三选一
        "dynamicCode": "手机验证码", //可选
        "privateKey": "私钥", //可选
        "storeCode": "存储群代码" //在平台里设立的存储群的群代码，后续可使用群做为成员管理
    }
}
```
#### 修改config.yaml配置KernelAuth对应的内容，然后直接dotnet run即可。


```
WorkerId: 1 //工作进程id
KernelNum: 1 //工作线程数
LogLevel: "Information" //日志等级，Critical,Error,Earning,Information,Debug,Trace,None
AllowedHosts: "*" //允许的主机地址，*为全部
Bucket: "/opt/bucket" // 文件类型存储位置
KeepAliveInterval: 2000 // 心跳间隔,不要改
Urls: "http://localhost:80" // 本地访问监听地址
FfmpegPath: "/opt/ffmpeg.exe" //ffmpeg可执行文件位置
ExecutorTimeout: 10000 //执行超时时间，小于10s
KernelAddr: "http://orginone.cn/orginone/resource/hub" // 关系核地址
ConnectionString: "Qs1dabW9uZ29kYjovL3Jvb3Q6MXFheiEyd3N4JTQwQDEwLjEwLjEwLjY4OjEwMDEvP2F1dGhTb3VyY2U9YWRtaW4=" // 数据库连接字符串
KernelAuth: "PPeYS3x00qAM4MY4uFUpU8IV1Zdsp53jhBUMHPWIhb89Quc/gD9W/45p08c24NgMdVD9wlLfjydYxit5zBc8wJpV/WWT2OXC" // 关系里数据核的认真key
```

### 参与贡献

1. fork 项目
   1. 首先，找到 fork 按钮，点击以后，你的存储内就会出现一个一模一样的项目。
2. 项目开发
   1. 按照奥集能项目的编码规则，对代码进行开发。
3. 跟上主项目的步伐
   1. 在你开发的过程中，主项目的代码也可能在更新。此时就需要你同步主项目的代码，找到 **Pull request** 按钮，点击。
   2. 在左侧选择你的存储的项目，右侧为主项目，此时你能在下面看到两个项目的区别，**点击 create pull request 按钮。**
   3. 填写 title，**点击 create pull request 按钮。**
   4. 进入 pull request 页面，拉到最下面，**点击 Merge pull request 按钮并确认，**现在你和主项目的代码就是同步的了。
4. Pull request
   1. 当你觉得你的代码开发完成，可以推送时，在确保你的修改全部推送到了你的存储的项目中，然后进入你的存储的项目页面，**点击 New pull request 按钮**，
   2. 然后**点击 create pull request 按钮**进行代码提交。
5. 审核
   1. 待项目的开发者审批完成之后，就是贡献成功了。


## 开发组构成
### 贡献者 Contributor
- 要求：完成一次pr的提交和合并
### 提交者 Committer
- 要求： 完成至少5个pr的提交和合并，并得到mentor的邀请
- 权力：review和accept pr
### 维护者 maintainer
- 要求：能长期维护项目，并得到mentor的邀请
- 权力：合并pr进入主分支
- Maintainer列表：  @realVeer @Socket_Captain
### 导师 Mentor
- 权力：决定项目发展方向，对项目进行指导
- 导师 @panzhaohui

